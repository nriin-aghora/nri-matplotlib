Matplotlib colorscheme that plays nice with NRI PPT template

## Installation

You could install `nri.mplstyle` matplotlib stylesheet as follows:

```bash
pip install -e .
```

## Manual Installation

Alternatively, you could install the stylesheet by putting "nri.mplstyle" in `~/.matplotlib/stylelib/` (or in Windows, `C:\Users\<USERNAME>\.matplotlib\stylelib`).
If the directory doesn't exist, create it manually.

Apply it in python as follows:

```python
import matplotlib.pyplot as plt
plt.style.use("nri")
```

## Example

![example](img/example.png)
