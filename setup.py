import atexit
import os
import shutil

import matplotlib
from setuptools import setup
from setuptools.command.install import install


def install_nri_mplstyle():
    stylefile = "nri.mplstyle"

    mpl_stylelib_dir = os.path.join(matplotlib.get_configdir(), "stylelib")
    if not os.path.exists(mpl_stylelib_dir):
        os.makedirs(mpl_stylelib_dir)

    print("Installing style into", mpl_stylelib_dir)
    shutil.copy(
        os.path.join(os.path.dirname(__file__), stylefile),
        os.path.join(mpl_stylelib_dir, stylefile),
    )


class PostInstallMoveFile(install):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        atexit.register(install_nri_mplstyle)


setup(
    name="nri-matplotlib",
    version="0.1.0",
    py_modules=["nri_matplotlib"],
    install_requires=[
        "matplotlib",
    ],
    cmdclass={
        "install": PostInstallMoveFile,
    },
)
